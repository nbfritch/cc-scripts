-- Return number of keys in table
function len(table)
  local keys = 0
  for k = 1, 16, 1 do
    if table[k] ~= nil then
      keys = keys + 1
    end
  end

  return keys
end

