function look()
  local current_slot = turtle.getSelectedSlot()
  local inv = {}
  for i = 1, 16, 1 do
    turtle.select(i)
    local d = turtle.getItemDetail()
    inv[i] = d
  end

  turtle.select(current_slot)
  return inv
end

function detail_diff(d1, d2)
  if (d1 == nil and d2 == nil) then
    return nil
  elseif (d1 == nil and d2 ~= nil) then
    return d2["name"]
  elseif d2 == nil and d1 ~= nil then
    return d1["name"]
  else
    if d1["name"] ~= d2["name"] then
      return d2["name"]
    elseif d1["count"] ~= d2["count"] then
      return d2["name"]
    else
      return nil
    end
  end
end

-- What did I get?
function wdig(i1, i2)
  local diffs = {}
  for i = 1, 16, 1 do
    local diff = detail_diff(i1[i], i2[i])
    if diff ~= nil then
      diffs[i] = diff
    end
  end
  return diffs
end

function what_changed(inv1, inv2)
  local diffs = {}
  for i = 1, 16, 1 do
    local i1 = inv1[i]
    local i2 = inv2[i]
    local diff = detail_diff(i1, i2)
    if diff ~= nil then
      diffs[i] = diff
    end
  end
end
